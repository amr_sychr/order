<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('products', 'ordercontroller@get_all_barang') ;
Route::post('products/product', 'ordercontroller@input_data_barang') ;
Route::put('/products/product/{productId}', 'ordercontroller@update_data_barang');
Route::delete('/products/product/{productId}', 'ordercontroller@hapus_data_barang');

Route::get('orders', 'ordercontroller@get_all_order') ;
Route::post('orders/order', 'ordercontroller@input_data_order') ;