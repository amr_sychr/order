<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangModel extends Model
{
    //
    protected $table = 'products' ;
    protected $primaryKey = 'productId';
    protected $guarded = [] ;
}
