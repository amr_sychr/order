<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BarangModel ;
use App\OrderModel ;

class ordercontroller extends Controller
{
    //
    public function get_all_barang () {
        return response()->json(BarangModel::all(), 200) ;
    }

    public function input_data_barang (Request $request) {
        $input_barang = new BarangModel ;
        $input_barang->nama_barang = $request->namaBarang ;
        $input_barang->kategori = $request->kategori ;
        $input_barang->jumlah_barang = $request->jumlahBarang ;
        $input_barang->harga_barang = $request->hargaBarang ;

        $input_barang->save();

        request()->validate([
            'namaBarang' => ['required'],
        ]);

        return response ([
            'Status' => 'OK', 
            'Pesan' => 'Data Barang Telah Disimpan', 
            'Data' => $input_barang
        ], 200) ;
    } 

    public function update_data_barang (request $request, $id) {
        $cek_barang = BarangModel::firstWhere('productId', $id) ;

        request()->validate([
            'namaBarang' => ['required'],
        ]);

        if ($cek_barang) {
            $data_barang = BarangModel::find($id) ;
            $data_barang->nama_barang = $request->namaBarang ;
            $data_barang->kategori = $request->kategori ;
            $data_barang->jumlah_barang = $request->jumlahBarang ;
            $data_barang->harga_barang = $request->hargaBarang ;
        
            $data_barang->save();

            return response ([
                'Status' => 'OK', 
                'Pesan' => 'Data Barang Telah Berhasil Diubah', 
                'Update_Data' => $data_barang
            ], 200) ;
        }
        else {
            return response ([
                'Status' => 'Not Found', 
                'Pesan' => 'Mohon Maaf, Data Barang Tidak Dapat Ditemukan'
            ], 404) ;
        }
    }
    
    public function hapus_data_barang (request $request, $id) {
        $cek_barang = BarangModel::firstWhere('productId', $id) ;

        if ($cek_barang) {
            BarangModel::destroy($id) ;
            return response ([
                'Status' => 'OK', 
                'Pesan' => 'Data Barang Telah Dihapus', 
            ], 200);
        }
        else {
            return response ([
                'Status' => 'Not Found', 
                'Pesan' => 'Mohon Maaf, Data Barang Tidak Dapat Ditemukan', 
            ], 200);
        }
    }

    public function get_all_order () {
        return response()->json(OrderModel::all(), 200) ;
    }
    
    public function input_data_order (Request $request) {
        $getBarang = new BarangModel ;
        $input_order = new OrderModel ;
        $input_order->id_user = $request->idUser ;
        $input_order->nama_barang = $request->namaBarang ;
        $input_order->kategori = $request->kategori ;
        $input_order->jumlah_barang = $request->jumlahBarang ;
        $input_order->harga_barang = $request->hargaBarang ;

        $input_order->save();

        request()->validate([
            'namaBarang' => ['required'],
        ]);

        return response ([
            'Status' => 'OK', 
            'Pesan' => 'Barang telah dipesan', 
            'Data' => $input_order
        ], 200) ;
    }
}
