<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class registercontroller extends Controller
{
    //
    public function action (Request $request) {
        $this->validate ($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:user|email',
            'password' => 'required|min:8',
        ]);
        $user = User::create ([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bycrpt($request->name),
            'api_token' => Str::random(80),
        ]);
        
        return response ()->json($user);
    }
}
